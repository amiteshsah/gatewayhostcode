package utility;

import java.io.Serializable;

public class InputParameterValues implements Serializable {
	
	private Long id;
	private Long dsId;
	private Long drId;
	private Long appId;
	private String type;
	private InputParamTypes inputParamType;
	private Long baapJobInsId;
	private String paramName;
	private String val;

	
	public InputParamTypes getInputParamType() {
		return inputParamType;
	}
	public void setInputParamType(InputParamTypes inputParamType) {
		this.inputParamType = inputParamType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getAppId() {
		return appId;
	}
	public void setAppId(Long appId) {
		this.appId = appId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDsId() {
		return dsId;
	}
	public void setDsId(Long dsId) {
		this.dsId = dsId;
	}
	public Long getDrId() {
		return drId;
	}
	public void setDrId(Long drId) {
		this.drId = drId;
	}
	public Long getBaapJobInsId() {
		return baapJobInsId;
	}
	public void setBaapJobInsId(Long baapJobInsId) {
		this.baapJobInsId = baapJobInsId;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appId == null) ? 0 : appId.hashCode());
		result = prime * result
				+ ((baapJobInsId == null) ? 0 : baapJobInsId.hashCode());
		result = prime * result + ((drId == null) ? 0 : drId.hashCode());
		result = prime * result + ((dsId == null) ? 0 : dsId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((inputParamType == null) ? 0 : inputParamType.hashCode());
		result = prime * result
				+ ((paramName == null) ? 0 : paramName.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((val == null) ? 0 : val.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InputParameterValues other = (InputParameterValues) obj;
		if (appId == null) {
			if (other.appId != null)
				return false;
		} else if (!appId.equals(other.appId))
			return false;
		if (baapJobInsId == null) {
			if (other.baapJobInsId != null)
				return false;
		} else if (!baapJobInsId.equals(other.baapJobInsId))
			return false;
		if (drId == null) {
			if (other.drId != null)
				return false;
		} else if (!drId.equals(other.drId))
			return false;
		if (dsId == null) {
			if (other.dsId != null)
				return false;
		} else if (!dsId.equals(other.dsId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inputParamType != other.inputParamType)
			return false;
		if (paramName == null) {
			if (other.paramName != null)
				return false;
		} else if (!paramName.equals(other.paramName))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (val == null) {
			if (other.val != null)
				return false;
		} else if (!val.equals(other.val))
			return false;
		return true;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InputParameterValues [id=");
		builder.append(id);
		builder.append(", dsId=");
		builder.append(dsId);
		builder.append(", drId=");
		builder.append(drId);
		builder.append(", appId=");
		builder.append(appId);
		builder.append(", type=");
		builder.append(type);
		builder.append(", inputParamType=");
		builder.append(inputParamType);
		builder.append(", baapJobInsId=");
		builder.append(baapJobInsId);
		builder.append(", paramName=");
		builder.append(paramName);
		builder.append(", val=");
		builder.append(val);
		builder.append("]");
		return builder.toString();
	}
	
}
