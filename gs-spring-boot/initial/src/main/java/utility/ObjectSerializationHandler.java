package utility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Serialize and deserialize the object into JSON string
 *
 * @author shiva
 *
 */
public class ObjectSerializationHandler {
	private static final Logger logger = LoggerFactory
			.getLogger(ObjectSerializationHandler.class);

	/**
	 * This will create the json string for the input object
	 *
	 * @param o
	 * @return
	 */
	public static String toString(Object o) {
		logger.info(" : >> toString()" + o);

		ObjectMapper mapper = new ObjectMapper();
		
		try {
			String writeValueAsString = mapper.writeValueAsString(o);

			logger.info(" : << toString()" + writeValueAsString);
			return writeValueAsString;
		} catch (JsonGenerationException e) {
			logger.info(e.toString());
			e.printStackTrace();
		} catch (JsonMappingException e) {
			logger.info(e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			logger.info(e.toString());
			e.printStackTrace();
		}
		logger.info(" : << toString()");
		return "";
	}
	/**
	 * This method is used to store date format correctly not in long format
	 * @param o
	 * @return
	 */
	public static String toStringForRunQuery(Object o) {
				logger.debug(" : >> toStringForRunQuery()" + o);
		
				ObjectMapper mapper = new ObjectMapper();
				
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

				try {
					String writeValueAsString = mapper.writeValueAsString(o);

					logger.info(" : << writeValueAsString()" + writeValueAsString);
					return writeValueAsString;
				} catch (JsonGenerationException e) {
					logger.info(e.toString());
					e.printStackTrace();
				} catch (JsonMappingException e) {
					logger.info(e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					logger.error(e.toString());
					e.printStackTrace();
				}
				return "";
			}

	/**
	 * This will create the object back from the json string.
	 *
	 */

	public static Object toObject(String str, Class<?> className) {
		logger.debug(" : >> toObject()" + str);

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

		mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

		mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
		
		mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);
		
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,
				true);

		mapper.setSerializationInclusion(Include.NON_NULL);

		try {
			logger.info(" : << toObject()",
					mapper.readValue(str, className));
			return mapper.readValue(str, className);

		} catch (Exception e) {
			str = "\"" + str + "\"";
			try {

				logger.info(" : << toObject()",
						mapper.readValue(str, className));
				return mapper.readValue(str, className);
			} catch (Exception e1) {

				// System.out.println("STR: " + str + "\nCLASS: " + className);
				logger.info("STR: " + str
						+ "\nCLASS: " + className);
				logger.error(e1.toString());
				e1.printStackTrace();
			}
		}
		logger.info(" : << toObject()");
		return null;
	}

	public static <T> List<T> toObjectList(String str, Class className, T typdef)
			throws JsonParseException, JsonMappingException, IOException {
		logger.info(" >> toObjectList()" + str);

		List<T> list = null;
		ObjectMapper mapper = new ObjectMapper();
		TypeFactory factory = TypeFactory.defaultInstance();

		list = mapper.readValue(str,
				factory.constructCollectionType(ArrayList.class, className));
		return list;
	}

	public static Map convertJsonToMap(String json) throws IOException {
		logger.info(" >> convertJsonToMap()" + json);

		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();

		mapper.configure(
				JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);

		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

		mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,
				true);
		
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		logger.info(" << convertJsonToMap()" + json);
		return mapper.readValue(json, new TypeReference<HashMap>() {
		});

	}
	
	public static Map convertLoggerStringToMap(String loggerStringJson){
		logger.info(" >> convertLoggerStringToMap()" + loggerStringJson);

		Map<String, String> loggerStingToJson = (Map<String, String>) ObjectSerializationHandler.toObject(loggerStringJson, Map.class);

		logger.info(" << convertLoggerStringToMap()" + loggerStingToJson);
		return loggerStingToJson;
	}
	
	public static String convertLoggerMapToString(Map<String, String> loggerJson){
		logger.info(" >> convertLoggerMapToString()" + loggerJson);
		
		String loggerJsonToString = ObjectSerializationHandler.toString(loggerJson);
		
		logger.info(" << convertLoggerMapToString()" + loggerJsonToString);
		return loggerJsonToString;
	}
}
