package utility;

public interface RequestStatus {

	String SUCCESS = "SUCCESS";
	String FAIL = "FAIL";
	String UNAUTHORISED_IP = "UNAUTHORISED_IP";
	String AUTHORISSATION_KEY_NOT_MATCHING = "AUTHORISSATION_KEY_NOT_MATCHING";
	String ALL_PACKETS_SENT = "All the packets have been sent sucessfully.";
	String AT_LEAST_ONE_PACKET_NOT_SENT = "Despite trying 5 times, at least one packet is not sent.";
	String TRANSFER_READY = "YES";
}