package utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AIQProperties extends Properties {

	private static final Logger logger = LoggerFactory.getLogger(AIQProperties.class);
	private static final Properties properties = new Properties();

	static {
		InputStream input = null;
		try {
			String file = "aiq.properies";
			InputStream inputStream = new FileInputStream(file);
			properties.load(inputStream);
			inputStream.close();


			Enumeration enuKeys = properties.keys();
			while (enuKeys.hasMoreElements()) {
				String key = (String) enuKeys.nextElement();
				String value = properties.getProperty(key);
				logger.info("AIQProperties::static::" + key + ": " + value);
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			
	}
	
	public static String get(String key) {
		return properties.getProperty(key);
	}
}