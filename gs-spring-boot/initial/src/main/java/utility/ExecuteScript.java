package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecuteScript {

    private static Logger logger = LoggerFactory.getLogger(ExecuteScript.class);

    private String command;
    private Long rootId;

    final static String EXPECTED_IMPL_CLASS_NAME = "java.lang.UNIXProcess";
    final static String EXPECTED_PID_FIELD_NAME = "pid";
    final static String SPACE = " ";
    

    public Long getRootId() {
	return rootId;
    }

    public void setRootId(Long rootId) {
	this.rootId = rootId;
    }

    private StringBuffer scriptOutput = new StringBuffer();

    public StringBuffer getScriptOutput() {
	return scriptOutput;
    }

    public void setScriptOutput(StringBuffer scriptOutput) {
	this.scriptOutput = scriptOutput;
    }

    public ExecuteScript(String command_) {
	this.command = command_;
    }

    public ExecuteScript(StringBuffer command_) {
	this.command = (null != command_ ? command_.toString() : "");
    }

    public String run() {
	logger.debug(LoggerConstants.LOG_AGENT + " >> run()");

	String execute = execute();

	logger.debug(LoggerConstants.LOG_AGENT + " << run()");

	return execute;
    }

	public String execute() {

		logger.debug(LoggerConstants.LOG_AGENT + " >> execute()");

		Process process;
		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;
		BufferedReader reader = null;
		try {

			logger.info(LoggerConstants.LOG_AGENT + "Running command: " + command);

			long startTime = System.currentTimeMillis();

			process = Runtime.getRuntime().exec(command);

			long endTime = System.currentTimeMillis();

			logger.info("Command: " + command + ". Took " + ((endTime - startTime) / 1000) + " seconds");

			Integer pidFromProcess = getPidFromProcess(process);

			logger.info(LoggerConstants.LOG_AGENT + " Command: " + command + " Pid of given shell script: "
					+ pidFromProcess);

			inputStream = process.getErrorStream();

			inputStreamReader = new InputStreamReader(inputStream);

			reader = new BufferedReader(inputStreamReader);

			String line = "";
			logger.info(LoggerConstants.LOG_AGENT + line);

			logger.info("\nShell output dump starts:-");
//			while ((line = reader.readLine()) != null) {
//				if (rootId != null && StringUtils.isNotBlank(line)) {
//					RunningLogService.sendMessageToWebForInfo(rootId + "", line);
//				}
//				scriptOutput.append(line);
//
//			}
			process.waitFor();

			InputStream outputStream = process.getInputStream();

			InputStreamReader outputStreamReader = new InputStreamReader(outputStream);

			BufferedReader outPutReader = new BufferedReader(outputStreamReader);

			String line1 = "";

			logger.info("\nShell input Reader output dump starts:-");
			while ((line1 = outPutReader.readLine()) != null) {
				scriptOutput.append(line1);

			}

			logger.info("Shell output dump ends!\n");

			return scriptOutput.toString();

		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			logger.warn(LoggerConstants.LOG_AGENT + e);

		} catch (InterruptedException e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			logger.warn(LoggerConstants.LOG_AGENT + e);

		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
			logger.warn(LoggerConstants.LOG_AGENT + e);

		} finally {
			if (null != inputStream) {
				try {
					inputStream.close();
				} catch (IOException e) {
					logger.debug(LoggerConstants.LOG_AGENT + " << execute()");
				}
			}

			if (null != inputStreamReader) {
				try {
					inputStreamReader.close();
				} catch (IOException e) {
					logger.debug(LoggerConstants.LOG_AGENT + " << execute()");
				}
			}

			if (null != reader) {
				try {
					reader.close();
				} catch (IOException e) {
					logger.debug(LoggerConstants.LOG_AGENT + " << execute()");
				}
			}
			logger.debug(LoggerConstants.LOG_AGENT + " << execute()");
		}
		return null;
	}

    /**
     * Get the process id (PID) associated with a {@code Process}
     * 
     * @param process
     *            {@code Process}, or null
     * @return Integer containing the PID of the process; -1 if the PID could
     *         not be retrieved or if a null parameter was supplied
     */

    public static Integer getPidFromProcess(Process process) {

	Integer pid = -1;

	try {

	    if (null == process)
		return pid;

	    Class<? extends Process> processImpl = process.getClass();

	    if (processImpl.getName().equals(EXPECTED_IMPL_CLASS_NAME)) {
		Field declaredField = process.getClass().getDeclaredField(EXPECTED_PID_FIELD_NAME);
		declaredField.setAccessible(true);
		pid = declaredField.getInt(process);
		declaredField.setAccessible(false);
	    } else {
		logger.info(Process.class.getName() + " implementation was not " + EXPECTED_IMPL_CLASS_NAME
			+ " - cannot retrieve PID" + " | actual type was: " + processImpl.getName());
	    }

	} catch (Exception e) {
	    logger.error("Unable to get pid ", e);
	    pid = -1;
	}

	return pid;
    }

}
