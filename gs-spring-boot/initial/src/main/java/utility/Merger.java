package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Merger {
 	public static void main(String[] args) {	
		String path = "/softwares/maxiq/maxiq/tmp/uploadpath/ID0.6081461793129324/";
		Merger merger = new Merger();
		merger.exec(path);	
	}
 	public final void exec(String path) {
	
		File mainFolder = new File(path);
		File[] files = mainFolder.listFiles();
		
		OutputStream out = null;

		try {
			String mergedFile = "merged.txt";
			//Creating New File
			File myFile = new File(path + mergedFile);
			myFile.createNewFile();
			out = new FileOutputStream(myFile);			 
			for (File eachFile:files) {
				InputStream in = null;
				try {
					byte[] buf = new byte[1024*1024];
					int len;
					in = new FileInputStream(eachFile);
					while ((len = in.read(buf)) > 0){
	                    out.write(buf, 0, len);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						in.close();
						eachFile.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}//end for
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
	}//end exec
}//end class