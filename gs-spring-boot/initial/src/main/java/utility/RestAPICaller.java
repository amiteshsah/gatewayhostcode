package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestAPICaller {

	private static final Logger logger = LoggerFactory
			.getLogger(RestAPICaller.class);

	public static String callRestApi(String targetUrl, byte[] bytes) throws IOException{
	    //logger.info("callRestApi() >> " + targetUrl);
		String str="";
		HttpURLConnection conn = null;
	        try {
	        
	        	URL url = new URL(targetUrl);
	            conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            conn.setRequestMethod("POST");
	            conn.setRequestProperty("Content-Type", "application/json");
                OutputStream os = conn.getOutputStream();
                os.write(bytes);    
                os.close();
	            BufferedReader br = null;
	            String output = "";
	            if (null != conn.getInputStream()) {
		            br = new BufferedReader(new InputStreamReader(
		                    (conn.getInputStream())));  
		            while ((output = br.readLine()) != null) {
		               str+=output;
		            }
	            }	            
	            logger.info("RestAPICaller::callRestApi::  Output is " + str);    
	            
	        } catch (MalformedURLException e) {
	        	logger.info("RestAPICaller::callRestApi::callRestApi() >> " + targetUrl);
	            e.printStackTrace();
	            throw e;
	        } catch (IOException e) {
	        	logger.info("RestAPICaller::callRestApi::callRestApi() >> " + targetUrl);
	            e.printStackTrace();
	            String response = "";
	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	            String line=null;
				while((line=in.readLine())!=null) {
					response = response + line;
				}				
				logger.info("RestAPICaller::callRestApi::response::" +   response);
	            throw e;
	        } finally {
	            conn.disconnect();
	        }	        
	        return str;
	    }

	public static String callCookieBasedRestApi(String targetUrl, byte[] bytes, String projectId) throws IOException{
	    logger.info("callRestApi() >> " + targetUrl);
		String str="";
		HttpURLConnection conn = null;
	        try {
	        
	        	URL url = new URL(targetUrl);
	            conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            conn.setRequestMethod("POST");
	            conn.setRequestProperty("Content-Type", "application/json");
	            conn.setRequestProperty("Cookie", "projectId="+projectId);
	            System.out.println("cokkieis done.................... set ------45----->>>>>>>>>> ");
                OutputStream os = conn.getOutputStream();
                os.write(bytes);    
                os.close();
	            BufferedReader br = null;
	            String output = "";
	            if (null != conn.getInputStream()) {
		            br = new BufferedReader(new InputStreamReader(
		                    (conn.getInputStream())));  
		            while ((output = br.readLine()) != null) {
		               str+=output;
		            }
	            }	            
	            logger.info("RestAPICaller::callRestApi::  Output is " + str);    
	            
	        } catch (MalformedURLException e) {
	        	logger.info("RestAPICaller::callRestApi::callRestApi() >> " + targetUrl);
	            e.printStackTrace();
	            throw e;
	        } catch (IOException e) {
	        	logger.info("RestAPICaller::callRestApi::callRestApi() >> " + targetUrl);
	            e.printStackTrace();
	            String response = "";
	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
	            String line=null;
				while((line=in.readLine())!=null) {
					response = response + line;
				}				
				logger.info("RestAPICaller::callRestApi::response::" +   response);
	            throw e;
	        } finally {
	            conn.disconnect();
	        }	        
	        return str;
	    }

}