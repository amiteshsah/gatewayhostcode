package controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dao.Dao;
import utility.AIQProperties;
import utility.CryptoUtil;
import utility.FileWriter;
import utility.GzipUtil;
import utility.InputParameterValues;
import utility.Merger;
import utility.ObjectSerializationHandler;
import utility.Packet;
import utility.PacketConsumer;
import utility.RequestStatus;
import utility.RestAPICaller;
import utility.SFTPSender;

@RestController
public class DataGatewayHostController {
	
	private static final String READY = "<<I am Ready>>";
	private static final Logger logger = LoggerFactory.getLogger(DataGatewayHostController.class);
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String uploadFile( HttpServletRequest request, @RequestParam String conncetionName) {
		
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}
		
		
		byte[] bytes = null;
		try {
			InputStream inputStream = request.getInputStream();
			bytes = IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			return RequestStatus.FAIL;
		}
		GzipUtil gzipUtil = new GzipUtil();
		String data = gzipUtil.decompress(bytes);
		CryptoUtil cryptoUtil=new CryptoUtil();
        data = cryptoUtil.decrypt(data);
        
        String readyId = getId(data);
        logger.info("DataGatewayHostController:uploadFile             >>>>>>>   readyId   <<<<<<<" + readyId);
        data = data.substring(data.indexOf(READY) + READY.length(), data.length());
//		logger.info("DataGatewayHostController:uploadFile           <<<<<<<<<<<<<<<<   From host  <<<<<   " + data + "                               <<<<<<<<<<<<<<<<<<<<");
		
		//API will get the path. plac change  
		String filePath = AIQProperties.get("httpsfilewritepath") + readyId;

		FileWriter fileWriter = new FileWriter();
		fileWriter.createFolder(filePath);

		Packet packet = new Packet(data.getBytes(), filePath + "/" + conncetionName + ".txt", readyId);
		
		try {
			Application.sharedQueue.put(packet);
          } catch (InterruptedException e) {
        	e.printStackTrace();
        }
		
					
		return RequestStatus.SUCCESS;
	}

	
	@RequestMapping(value = "/getUploadPath", method = {RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String getUploadPath( HttpServletRequest request) {
				
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}
		
		String path = AIQProperties.get("SFTP_PATH");
		return path;
	}	
	
	@RequestMapping(value = "/areYouReady", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String areYouReady(HttpServletRequest request) {
		
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}
		
		double random = Math.random();
		logger.info("DataGatewayHostController:areYouReady:random"+ random);
//		if (random > .5) return RequestStatus.FAIL;
		
		return "ID" + random;// + READY;
	}
	
	
	@RequestMapping(value = "/getRecivedPacketsCountSFTP", method = { RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String getRecivedPacketsCountSFTP(HttpServletRequest request) {
		
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}
        
		String readyId = null;
		try {
			InputStream inputStream = request.getInputStream();
			byte[] bytes = IOUtils.toByteArray(inputStream);
			readyId = new String(bytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			return "-1";
		}
		
		if (null == readyId)
			return "-1";
			
        readyId = getId(readyId);
        FileWriter fileWriter = new FileWriter();
        String fileCountInFolder = "" + fileWriter.getFileCountInFolder(AIQProperties.get("SFTP_PATH") + readyId);
        logger.info("DataGatewayHostController:getRecivedPacketsCountSFTP       ---sftp-----fileCountInFolder--->>" + fileCountInFolder);
        logger.info("DataGatewayHostController:getRecivedPacketsCountSFTP       ---Merger started");
		Merger merger = new Merger();
		merger.exec(AIQProperties.get("SFTP_PATH") + readyId + "/");	
        logger.info("DataGatewayHostController:getRecivedPacketsCountSFTP       ---Merger ended.");
        return fileCountInFolder;
	}

	@RequestMapping(value = "/getRecivedPacketsCountHTTPS", method = { RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String getRecivedPacketsCountHTTPS(HttpServletRequest request) {
		
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}
        
		String readyId = null;
		try {
			InputStream inputStream = request.getInputStream();
			byte[] bytes = IOUtils.toByteArray(inputStream);
			readyId = new String(bytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			return "-1";
		}
		
		if (null == readyId)
			return "-1";
		
		readyId = getId(readyId);
		String httpWrittenPacketsCount = PacketConsumer.wittenPacketCountMap.get(readyId).toString();
	
		//Here, we will transfer the files from host to Mosaic Web.
		String SHELL_SCRIPT_PATH = AIQProperties.get("SHELL_SCRIPT_PATH");
		String SFTP_USER = AIQProperties.get("SFTP_USER");
		String SFTP_SERVER = AIQProperties.get("SFTP_SERVER");
		String SFTP_PASSWORD = AIQProperties.get("SFTP_PASSWORD");
		String SFTP_PATH = AIQProperties.get("SFTP_PATH");
		String hostLocalPath = AIQProperties.get("httpsfilewritepath");
		SFTPSender.scpFiles(SHELL_SCRIPT_PATH, SFTP_USER, SFTP_SERVER, SFTP_PASSWORD, SFTP_PATH + readyId, hostLocalPath + readyId);
		System.out.println("  >>>>>>>>   call to sendFilesfromDirectory() is complete  <<<<<<<<<<<<");
	
		
		logger.info("DataGatewayHostController: getRecivedPacketsCountHTTPS    ---https-----httpWrittenPacketsCount--->>" + httpWrittenPacketsCount);
		return httpWrittenPacketsCount;
	}

	
	
	@RequestMapping(value = "/sendDataSource", method = { RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String sendDataSource(HttpServletRequest request) {
		
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}

		try {
			InputStream inputStream = request.getInputStream();
			byte[] bytes = IOUtils.toByteArray(inputStream);
			logger.info("DataGatewayHostController: getRecivedPacketsCountHTTPS            .............callg...........");
			String weburl = AIQProperties.get("weburl") + "compareDataSources";
			String status = RestAPICaller.callRestApi(weburl, bytes);
			logger.info(status);
			return status;
		} catch (IOException e) {
			e.printStackTrace();
			return "Error";
		}
	}
			
	
	@RequestMapping(value = "/runDataSource", method = { RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String runDataSource(HttpServletRequest request, @RequestParam Long dataSourceId, @RequestBody List<InputParameterValues> inputParams) {
		
		if (isUnAuthorised(request)) {
			return RequestStatus.UNAUTHORISED_IP;
		}
		
        logger.info("DataGatewayHostController::runDataSource::dataSourceId" +   dataSourceId);
        String projectId = new Dao().findProjectId(dataSourceId.toString());
        System.out.println("  >>>>>>>>   projectId  <<<<<<<<<<<<" + projectId);
        
		try {
			/*
			 * This code will print the values of data in the list.
			 */
			for(InputParameterValues eachParameterValues : inputParams) {
				logger.info(eachParameterValues.toString());
			}
			
			logger.info("DataGatewayHostController::runDataSource:::");
			String weburl = AIQProperties.get("weburl") + "runDataSource?dataSourceId=" + dataSourceId;
			String status = RestAPICaller.callCookieBasedRestApi(weburl, ObjectSerializationHandler.toString(inputParams).getBytes(), projectId);
			logger.info(status);
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
			
	private final boolean isUnAuthorised(HttpServletRequest request) {
		String remoteAddr = request.getHeader("X-FORWARDED-FOR");
		logger.info("DataGatewayHostController::isUnAuthorised The request was sent by this machine ip address:" + remoteAddr);
        if (null == remoteAddr || "".equals(remoteAddr)) {
            remoteAddr = request.getRemoteAddr();
            logger.info("DataGatewayHostController::isUnAuthorised The request was sent by this machine firewall ip address:" + remoteAddr);
        }

        if (null != remoteAddr) {
        	remoteAddr = remoteAddr.trim();
        }
        
//        if (!AIQProperties.get("agentip").equals(remoteAddr)) {
        if (AIQProperties.get("agentip").indexOf(remoteAddr) == -1) {
        	logger.info("DataGatewayHostController::isUnAuthorised Request came up from Unauthorised Agent machine. The ip address is not matching.");
        	return true;
        }
        return false;
	}	
	
	
	@RequestMapping(value = "/isipnotregistered", method = { RequestMethod.POST, RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public String isipnotregistered( HttpServletRequest request, @RequestParam String autherizationKey) {
		System.out.println("DataGatewayHostController::isipnotregistered ---autherizationKey> " +  autherizationKey);
		logger.info("DataGatewayHostController::isipnotregistered ---autherizationKey> " +  autherizationKey);
		
		if (!AIQProperties.get("AUTHENTICATION_KEY").equals(autherizationKey))
			return RequestStatus.AUTHORISSATION_KEY_NOT_MATCHING;
		if (isUnAuthorised(request))
			return RequestStatus.UNAUTHORISED_IP;
		
		return RequestStatus.SUCCESS;
	}
	
	private String getId (String id) {
		if (null == id)
			return null;
        return id.substring(0, id.indexOf(READY));		
	}	
}